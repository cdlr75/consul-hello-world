## Leader election

We have an App, `app.py`.

What we want:
- many App instances
- a single leader amoung those instances
- leader role is acquired by another instance if the leader die (gracefull shutdown or not)
